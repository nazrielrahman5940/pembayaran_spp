<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Pembayaran;
use App\Models\Siswa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function GuzzleHttp\Promise\all;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $siswa = DB::table('siswa')->count();
        $kelas = DB::table('kelas')->count();
        $user = DB::table('users')->count();
        // $totalpendapatan = DB::table('jumlah_bayar')->count();
        $totalpendapatan = Pembayaran::sum('jumlah_bayar');
        
        // dd($siswa);
        // $user = User::all();
        // $pembayaran = Pembayaran::all();
        // $kelas = Kelas::all();
        return view('home', compact('siswa','kelas','user','totalpendapatan'));
    }
}
