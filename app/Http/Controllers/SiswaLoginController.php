<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SiswaLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('auth-siswa.login');
    }

    public function actionlogin(Request $request)
    {
        // dd($request->all());

        try {
            //cek apakah nisn dan nama yg diinput ada di DB ?
            $siswa = Siswa::where([
                'nisn' => $request->nisn,
                'nama' => $request->nama,
            ])->first();
            // $creds = [
            //     'nisn' => $request->nisn,
            //     'nama' => $request->nama,
            //     'password' => 1337,
            // ];
            // $attempt = Auth::guard('siswa')->attempt(
            //     $creds,
            //     $request->get('remember')
            // );
            // dd($attempt);

            if (!empty($siswa)) { //klo ada masuk sni
                Session::put('is_siswa', 1);
                Auth::guard('siswa')->login($siswa);

                return redirect()->route('siswa.histori');
            } else { // klo g, gagal login
                return redirect()->to('/siswa/login')->withErrors(['error' => 'NISN / Nama tidak ditemukan pada record !']);
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
