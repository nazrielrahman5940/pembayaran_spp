<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class kelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kelas = Kelas::all();
        $kelas = DB::table('kelas')->get();
        return view('layouts.kelas.index', compact('kelas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.kelas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kelas' => ['required', 'string'],
            'keahlian' => ['required', 'string', 'max:255'],
        ]);
         try{
            $user = new Kelas();
            $user->kelas = $request->kelas;
            $user->keahlian = $request->keahlian;
            $user->save();
       }
        catch(\Exception $e ){

            return redirect()->back()->withErrors(['Kelas gagal disimpan']);
        }
        return redirect('kelas')->with('status','Kelas Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelas = Kelas::find($id);
        return view('layouts.kelas.edit', compact('kelas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kelas' => ['required', 'string'],
            'keahlian' => ['required', 'string', 'max:255'],
        ]);
         try{
            $user = Kelas::find($id);
            $user->kelas = $request->kelas;
            $user->keahlian = $request->keahlian;
            $user->save();
       }
        catch(\Exception $e ){

            return redirect()->back()->withErrors(['Kelas gagal disimpan']);
        }
        return redirect('kelas')->with('status','Kelas Berhasil ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Kelas::find($id);
        $user->delete();
         
         return redirect()->back()->with('status','kelas berhasil dihapus');
    }
}
