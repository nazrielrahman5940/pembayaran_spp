<?php

namespace App\Http\Controllers;

use App\Models\Pembayaran;
use App\Models\Siswa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class pembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $totalpendapatan = DB::table('jumlah_bayar')->get();
        $pembayaran = Pembayaran::with('user', 'siswa')->get();
        return view('layouts.pembayaran.index', compact('pembayaran',));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $siswa = Siswa::all();
        $user = User::all();
        return view('layouts.pembayaran.create', compact('user', 'siswa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nisn' => ['required', 'numeric'],
            'spp_bulan' => ['required'],
            'jumlah_bayar' => ['required', 'numeric'],
        ]);

        DB::beginTransaction();
        try {
            // dd($request->all());
            $nisn = $request->nisn;
            $siswa = Siswa::whereNisn($nisn)->first();
            $user = new Pembayaran();
            $user->id_user = Auth::id();
            $user->id_siswa = $siswa->id;
            $user->spp_bulan = $request->spp_bulan;
            $user->jumlah_bayar = $request->jumlah_bayar;
            $user->save();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors(['Data Pembayaran gagal disimpan'])->withInput();
        }
        return redirect('pembayaran')->with('status', 'Data Pembayaran berhasil di simpan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pembayaran = Pembayaran::find($id);
        $siswa = Siswa::all();
        $user = User::all();
        return view('layouts.pembayaran.edit', [
            'pembayaran' => $pembayaran,
            'siswa' => $siswa,
            'user' => $user,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nisn' => ['required', 'numeric'], //cba kamu cek lgi cek apanya
            'spp_bulan' => ['required'],
            'jumlah_bayar' => ['required', 'numeric'],
        ]);

        try {
            // dd($request->all());
            $nisn = $request->nisn;
            $siswa = Siswa::whereNisn($nisn)->first();
            $user = Pembayaran::find($id);
            $user->id_user = Auth::id();
            $user->id_siswa = $siswa->id;
            $user->spp_bulan = $request->spp_bulan;
            $user->jumlah_bayar = $request->jumlah_bayar;
            $user->save();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['Data Siswa gagal disimpan'])->withInput();
        }
        return redirect('pembayaran')->with('status', 'Data Siswa berhasil di simpan.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generatelaporan()
    {
        return view('layouts.laporan.index');
    }
    public function generatelaporancetak($tglawal, $tglakhir)
    {
        // dd(["Tanggal Awal : ".$tglawal, "Tanggal Akhir : ".$tglakhir]);
        $pembayaran = Pembayaran::with('siswa')->whereBetween('created_at',[$tglawal, $tglakhir])->oldest()->get();
        $totalpendapatan = Pembayaran::sum('jumlah_bayar');
        return view('layouts.laporan.generate-laporan-cetak', compact('pembayaran','totalpendapatan','tglawal','tglakhir'));
    }

}
