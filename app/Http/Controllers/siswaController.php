<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Spp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class siswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswa = DB::table('siswa')->get();
        // dd($siswa);
        $siswa = Siswa::with('kelas')->get();
        return view('layouts.siswa.index', compact('siswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelas = Kelas::all();
        $spp = Spp::all();
        return view('layouts.siswa.create', [
            'kelas' => $kelas,
            'spp' => $spp
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nisn' => ['required', 'unique:siswa', 'numeric'],
            'nis' => ['required', 'numeric'],
            'nama' => ['required'],
            // 'kelas' => ['required','integer'],
            'nomor_telp' => ['required', 'numeric'],
            'alamat' => ['required'],
            // 'spp' => ['required','integer'],
        ]);

        try {
            // dd($request->all());
            $user = new Siswa();
            $user->nisn = $request->nisn;
            $user->nis = $request->nis;
            $user->nama = $request->nama;
            $user->id_kelas = $request->id_kelas;
            $user->nomor_telp = $request->nomor_telp;
            $user->alamat = $request->alamat;
            $user->id_spp = $request->id_spp;
            $user->save();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['Data Siswa gagal disimpan'])->withInput();
        }
        return redirect('siswa')->with('status', 'Data Siswa berhasil di simpan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswa = Siswa::find($id);
        $kelas = Kelas::all();
        $spp = Spp::all();
        return view('layouts.siswa.edit', [
            'siswa' => $siswa,
            'kelas' => $kelas,
            'spp' => $spp,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nisn' => ['required', 'numeric'],
            'nis' => ['required', 'numeric'],
            'nama' => ['required'],
            'nomor_telp' => ['required', 'numeric'],
            'alamat' => ['required'],
        ]);

        try {
            // dd($request->all());
            $user = Siswa::find($id);
            $user->nisn = $request->nisn;
            $user->nis = $request->nis;
            $user->nama = $request->nama;
            $user->id_kelas = $request->id_kelas;
            $user->nomor_telp = $request->nomor_telp;
            $user->alamat = $request->alamat;
            $user->id_spp = $request->id_spp;
            $user->save();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['Data Siswa gagal disimpan'])->withInput();
        }
        return redirect('siswa')->with('status', 'Data Siswa berhasil di simpan.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Siswa::find($id);
        $user->delete();
         
         return redirect()->back()->with('status','Siswa berhasil dihapus');
    }
}
