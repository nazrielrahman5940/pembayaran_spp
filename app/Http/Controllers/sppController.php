<?php

namespace App\Http\Controllers;

use App\Models\Spp;
use Illuminate\Http\Request;

class sppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spp = Spp::all();
        return view('layouts.spp.index', compact('spp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.spp.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tahun' => ['required','min:4','max:4'],
            'nominal' => ['required', 'numeric'],
        ]);
         try{
            $user = new Spp();
            $user->tahun = $request->tahun;
            $user->nominal = $request->nominal;
            $user->save();
       }
        catch(\Exception $e ){

            return redirect()->back()->withErrors(['Spp gagal disimpan']);
        }
        return redirect('spp')->with('status','Spp Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $spp = Spp::find($id);
        return view('layouts.spp.edit', compact('spp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tahun' => ['required','min:4','max:4'],
            'nominal' => ['required', 'numeric'],
        ]);
         try{
            $user = Spp::findOrFail($id);
            $user->tahun = $request->tahun;
            $user->nominal = $request->nominal;
            $user->save();
       }
        catch(\Exception $e ){

            return redirect()->back()->withErrors(['Spp gagal disimpan']);
        }
        return redirect('spp')->with('status','Spp Berhasil ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Spp::find($id);
        $user->delete();
         
         return redirect()->back()->with('status','spp berhasil dihapus');
    }
}
