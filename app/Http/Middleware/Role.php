<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $roles = array_slice(func_get_args(), 2);

        if (Auth::guest() && !session()->has('is_siswa')) {
            return redirect()->to('/login');
        }

        foreach ($roles as $roles) {

            if (!session()->has('is_siswa')) {
                $user = Auth::user()->level;

                if ($user == $roles) {
                    return $next($request);
                }
            } else {

                if ($roles == 'siswa') {
                    return $next($request);
                }
            }
        }

        return redirect('/dashboard');
    }
}
