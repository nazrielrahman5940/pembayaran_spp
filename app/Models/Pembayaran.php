<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    use HasFactory;
    
    protected $table = 'pembayaran';

    protected $fillable = [
        'id_user',
        'id_siswa', 
        'spp_bulan', 
        'jumlah_bayar'
    ];


    protected $hidden = [];

    public function siswa()
   {
       return $this->belongsTo(Siswa::class, 'id_siswa', 'id');
   }

   public function user()
   {
       return $this->belongsTo(User::class, 'id_user', 'id', 'nisn' ); 
   }
}
