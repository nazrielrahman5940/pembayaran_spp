<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spp extends Model
{
    use HasFactory;
    protected $table = 'spp';

    protected $fillable = [
        'tahun', 
        'nominal'
    ];


    protected $hidden = [];

    public function siswa(){
        return $this->hasMany(Siswa::class, 'id_spp');
    }
}
