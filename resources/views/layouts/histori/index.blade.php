
@extends('adminlte::page')
@section('title', 'Halaman Siswa')

@section('content_header')
    <h1 class="m-0 text-dark">histori</h1>
@stop
@section('link')
    <li class="breadcrumb-item active">Histori</li>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            @if (session('status'))
                <x-adminlte-alert theme="success" title="Sukses">
                    {{ session('status') }}
                </x-adminlte-alert>
            @endif
            @if ($errors->any())
                <x-adminlte-alert theme="success" title="Sukses">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-adminlte-alert>
            @endif
           
                        @foreach ($pembayaran as $value)
                        <section class="content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                        
                                        <div class="card">
                                            <div class="card-header">
                                                <h3 class="card-title">{{ $value->siswa->nama .' - '. $value->siswa->kelas->kelas }}</h3>
                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                                        <i class="fas fa-minus"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                                        <i class="fas fa-times"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div>SPP Bulan <b class="text-capitalize">{{ $value->spp_bulan }}</b></div>
                                                <div>Nominal SPP Rp.{{ $spp = $value->siswa->spp->nominal }}</div>
                                                <div>Bayar Rp.{{ $bayar = $value->jumlah_bayar }}</div>
                                                <div>Tunggakan Rp.{{ $spp - $bayar }}</div>   
                                            </div>
                        
                                            <div class="card-footer">
                                                {{ $value->created_at->format('d M, Y') }}
                                            </div>
                        
                                        </div>
                        
                                    </div>
                                </div>
                            </div>
                        </section>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
        $('#tabel_siswa').DataTable();
    </script>
@stop

