@extends('adminlte::page')
@section('title', 'Halaman Ubah Kelas')
@section('content_header')
   <h1>Ubah Kelas</h1>
@stop
@section('link')
<li class="breadcrumb-item"><a href="{{route('kelas.index')}}">Kelas</a></li>
<li class="breadcrumb-item active">Ubah</li>
@stop
@section('content')
<div class="card">
   <div class="card-body">
       <form method="POST" action="{{ route('kelas.update', $kelas->id) }}" >
        @if ($errors->any())
                <x-adminlte-alert theme="danger" title="Kesalahan">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-adminlte-alert>
            @endif
        @method('PUT')
           @csrf
           <div class="row mb-3">
               <label for="kelas" class="col-md-4 col-form-label text-mdend">{{ __('Kelas') }}</label>
                   <div class="col-md-6">
                       <input id="kelas" type="text" class="form-control @error('kelas') is-invalid @enderror" name="kelas" value="{{ $kelas->kelas}}" required autofocus>
                           @error('kelas')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                   </div>
           </div>
           <div class="row mb-3">
               <label for="keahlian" class="col-md-4 col-form-label text-md-end">{{ __('Keahlian') }}</label>
                   <div class="col-md-6">
                       <input id="keahlian" type="text" class="form-control @error('keahlian') is-invalid @enderror" name="keahlian" value="{{ $kelas->keahlian}}"required autocomplete="name" autofocus>
                           @error('keahlian')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                   </div>
           </div>
           <div class="row mb-0">
               <div class="col-md-6 offset-md-4">
                   <button type="submit" class="btn btn-primary">
                                   Simpan
                   </button>
           </div>
       </div>
   </form>
   </div>
</div>
@endsection