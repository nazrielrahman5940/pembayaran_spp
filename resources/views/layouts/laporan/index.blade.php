@extends('adminlte::page')
@section('title', 'Halaman Kelas')

@section('content_header')
   <h1 class="m-0 text-dark">Generate Laporan</h1>
@stop
@section('link')
<li class="breadcrumb-item active">Generate Laporan</li>
@stop

@section('content')
    <div class="row">
        @if (session('status'))
           <x-adminlte-alert theme="success" title="Sukses">
               {{session('status')}}
           </x-adminlte-alert>
       @endif
       @if ($errors->any())
           <x-adminlte-alert theme="success" title="Sukses">
               <ul>
                   @foreach ($errors->all() as $error)
                       <li>{{ $error }}</li>
                   @endforeach
               </ul>
           </x-adminlte-alert>
       @endif
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">Laporan Pembayaran</div>
                <div class="card-body">
                    <form  action="">
                        @csrf
                        <div class="form-group">
                            <label for="tglawal">Tanggal Mulai</label>
                            <input type="date" name="tglawal" id="tglawal" required="" class="form-control" id="tanggal_mulai">
                        </div>
                        <div class="form-group">
                            <label for="tglakhir">Tanggal Selesai</label>
                            <input type="date" name="tglakhir" id="tglakhir" required="" class="form-control" id="tanggal_selesai">
                        </div>
                        <div class="form-group">
                        <a href="" onclick="this.href='/generate-laporan-cetak/'+ document.getElementById('tglawal').value +
                            '/' +document.getElementById('tglakhir').value" target="_blank" class="btn btn-danger btn-sm">
                                <i class="fas fa-print fa-fw"></i> PRINT
                        </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>	
    </div>


@stop
@section('plugins.Datatables', true)
@section('js')
   <script> $('#tabel_kelas').DataTable();</script>
@stop