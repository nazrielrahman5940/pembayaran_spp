@extends('adminlte::page')
@section('title', 'Halaman Tambah Pembayaran')
@section('content_header')
    <h1>Tambah Pembayaran</h1>
@stop
@section('link')
    <li class="breadcrumb-item"><a href="{{ route('pembayaran.index') }}">Pembayaran</a></li>
    <li class="breadcrumb-item active">Tambah</li>
@stop
@section('content')
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
                <x-adminlte-alert theme="danger" title="Kesalahan">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-adminlte-alert>
            @endif
            <form method="POST" action="{{ route('pembayaran.store') }}">
                @csrf
                <div class="row mb-3">
                    <label for="nisn" class="col-md-4 col-form-label text-mdend">{{ __('NISN Siswa') }}</label>
                    <div class="col-md-6">
                        <input id="nisn" type="text" class="form-control @error('nisn') is-invalid @enderror"
                            name="nisn" value="{{ old('nisn') }}" required autocomplete="nisn" autofocus>
                        @error('nisn')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="spp_bulan" class="col-md-4 col-form-label textmd-end">{{ __('SPP Bulan') }}</label>
                    <div class="col-md-6">
                        <select id="spp_bulan" class="form-control @error('spp_bulan') is-invalid @enderror" 
                        name="spp_bulan" required>

                        <option value="">Silahkan Pilih</option>											
                        <option value="januari">Januari</option>
                        <option value="februari">Februari</option>
                        <option value="maret">Maret</option>
                        <option value="april">April</option>
                        <option value="mei">Mei</option>
                        <option value="juni">Juni</option>
                        <option value="juli">Juli</option>
                        <option value="agustus">Agustus</option>
                        <option value="september">September</option>
                        <option value="oktober">Oktober</option>
                        <option value="november">November</option>
                        <option value="desember">Desember</option>
                            
                        </select>
                        @error('spp_bulan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                

                <div class="row mb-3">
                    <label for="jumlah_bayar" class="col-md-4 col-form-label text-mdend">{{ __('Jumlah Bayar') }}</label>
                    <div class="col-md-6">
                        <input id="jumlah_bayar" type="text" class="form-control @error('jumlah_bayar') is-invalid @enderror"
                            name="jumlah_bayar" value="{{ old('jumlah_bayar') }}" required autofocus>
                        @error('jumlah_bayar')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Simpan
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
