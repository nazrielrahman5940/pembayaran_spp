@extends('adminlte::page')
@section('title', 'Halaman Tambah Siswa')
@section('content_header')
    <h1>Tambah Siswa</h1>
@stop
@section('link')
    <li class="breadcrumb-item"><a href="{{ route('siswa.index') }}">Siswa</a></li>
    <li class="breadcrumb-item active">Tambah</li>
@stop
@section('content')
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
                <x-adminlte-alert theme="danger" title="Kesalahan">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-adminlte-alert>
            @endif
            <form method="POST" action="{{ route('siswa.store') }}">
                @csrf

                <div class="row mb-3">
                    <label for="nisn" class="col-md-4 col-form-label text-mdend">{{ __('Nisn') }}</label>
                    <div class="col-md-6">
                        <input id="nisn" type="text" class="form-control @error('nisn') is-invalid @enderror"
                            name="nisn" value="{{ old('nisn') }}" required autocomplete="nama" autofocus>
                        @error('nisn')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="nis" class="col-md-4 col-form-label text-mdend">{{ __('Nis') }}</label>
                    <div class="col-md-6">
                        <input id="nis" type="text" class="form-control @error('nis') is-invalid @enderror"
                            name="nis" value="{{ old('nis') }}" required autocomplete="nama" autofocus>
                        @error('nis')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="nama" class="col-md-4 col-form-label text-mdend">{{ __('Nama') }}</label>
                    <div class="col-md-6">
                        <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror"
                            name="nama" value="{{ old('nama') }}" required autocomplete="nama" autofocus>
                        @error('nama')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="id_kelas" class="col-md-4 col-form-label textmd-end">{{ __('Kelas') }}</label>
                    <div class="col-md-6">
                        <select id="id_kelas" class="form-control @error('id_kelas') is-invalid @enderror"
                        {{ count($kelas) == 0 ? 'disabled' : '' }} name="id_kelas" required>
                        @if (count($kelas) == 0)
                            <option>Pilihan Tidak ada</option>
                        @else
                            @foreach ($kelas as $k)
                            <option value="{{ $k->id }}"> {{ $k->kelas }}</option>
                            @endforeach
                        @endif
                            
                        </select>
                        @error('id_kelas')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                

                <div class="row mb-3">
                    <label for="nomor_telp" class="col-md-4 col-form-label text-mdend">{{ __('Nomor_Telp') }}</label>
                    <div class="col-md-6">
                        <input id="nomor_telp" type="text" class="form-control @error('nomor_telp') is-invalid @enderror"
                            name="nomor_telp" value="{{ old('nomor_telp') }}" required autofocus>
                        @error('nomor_telp')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="alamat" class="col-md-4 col-form-label textmd-end">{{ __('Alamat') }}</label>
                    <div class="col-md-6">
                        <textarea name="alamat" class="form-control @error('alamat') is-invalid @enderror">{{ old('alamat') }}</textarea>
                        @error('alamat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="id_spp" class="col-md-4 col-form-label textmd-end">{{ __('Spp') }}</label>
                    <div class="col-md-6">
                        <select id="id_spp" class="form-control @error('id_spp') is-invalid @enderror"
                        {{ count($spp) == 0 ? 'disabled' : '' }} name="id_spp" required>
                        @if(count($spp) == 0)											
                              <option>Pilihan tidak ada</option>
                        @else				
                            @foreach ($spp as $p)
                                <option value="{{ $p->id }}">{{ 'Tahun '.$p->tahun.' - '.'Rp.'.$p->nominal }}</option>
                            @endforeach
                        @endif
                        </select>
                        @error('id_spp')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Simpan
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
