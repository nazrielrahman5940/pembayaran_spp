@extends('adminlte::page')
@section('title', 'Halaman Tambah Spp')
@section('content_header')
   <h1>Tambah Spp</h1>
@stop
@section('link')
<li class="breadcrumb-item"><a href="{{route('spp.index')}}">Spp</a></li>
<li class="breadcrumb-item active">Tambah</li>
@stop
@section('content')
<div class="card">
   <div class="card-body">
    @if ($errors->any())
                <x-adminlte-alert theme="danger" title="Kesalahan">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-adminlte-alert>
            @endif
       <form method="POST" action="{{ route('spp.store') }}" >
           @csrf
           <div class="row mb-3">
               <label for="tahun" class="col-md-4 col-form-label text-mdend">{{ __('Tahun') }}</label>
                   <div class="col-md-6">
                       <input id="tahun" type="text" class="form-control @error('tahun') is-invalid @enderror" name="tahun" value="{{old('kelas') }}" required autocomplete="name" autofocus>
                           @error('tahun')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                   </div>
           </div>
           <div class="row mb-3">
               <label for="nominal" class="col-md-4 col-form-label text-md-end">{{ __('Nominal') }}</label>
                   <div class="col-md-6">
                       <input id="nominal" type="text" class="form-control @error('nominal') is-invalid @enderror" name="nominal" value="{{ old('nominal') }}"required autocomplete="name" autofocus>
                           @error('nominal')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                   </div>
           </div>
           <div class="row mb-0">
               <div class="col-md-6 offset-md-4">
                   <button type="submit" class="btn btn-primary">
                                   Simpan
                   </button>
           </div>
       </div>
   </form>
   </div>
</div>
@endsection