@extends('adminlte::page')
@section('title', 'Halaman Spp')

@section('content_header')
   <h1 class="m-0 text-dark">Spp</h1>
@stop
@section('link')
<li class="breadcrumb-item active">Spp</li>
@stop

@section('content')
<div class="card">
   <div class="card-body">
       @if (session('status'))
           <x-adminlte-alert theme="success" title="Sukses">
               {{session('status')}}
           </x-adminlte-alert>
       @endif
       @if ($errors->any())
           <x-adminlte-alert theme="success" title="Sukses">
               <ul>
                   @foreach ($errors->all() as $error)
                       <li>{{ $error }}</li>
                   @endforeach
               </ul>
           </x-adminlte-alert>
       @endif
       <a href="{{route('spp.create')}}" class="btn btn-md btn-success mx-1 shadow"> <i class="fa fa-lg fa-fw fa-plus"></i> Tambah Kelas</a>
       <br/> <br/>
       <div class="table-responsive">
           <table id="tabel_kelas" class="table table-striped table-hover table-condensed table-bordered">
               <thead>
                   <tr style="background-color: darkgrey">
                       <th>No</th>
                       <th>Tahun </th>
                       <th>Nominal</th>
                       <th class="text-center" width="100">Aksi</th>
                   </tr>
               </thead>
               <tbody>
               @foreach($spp as $p)
                   <tr>
                       <td>{{$loop->iteration}}</td>
                       <td>{{$p->tahun}}</td>
                       <td>{{$p->nominal}}</td>
                       <td>
                        <a href="{{route('spp.edit',$p->id)}}" class="btn btn-sn btn-primary"
                            title="Edit"><i class="far fa-edit"></i></a>
                            <x-adminlte-button class="btn btn-sn btn-danger" data-toggle="modal" data-target="#hapusspp{{$loop->iteration}}" icon="far fa-trash-alt" class="bg-danger"> Hapus </x-adminlte-button>
                                {{-- Custom --}}
                            <x-adminlte-modal id="hapusspp{{$loop->iteration}}" title="Hapus Spp" size="md" theme="danger"
                            icon="fas fa-bell" v-centered static-backdrop scrollable>
                            <div style="height:80px;">
                                <form action="{{route('spp.delete',$p->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                      Apakah anda yakin akan menghapus spp ini?</div>
                            <x-slot name="footerSlot">
                                <x-adminlte-button type="submit" class="mr-auto" theme="primary" label="Hapus"/>
                                <x-adminlte-button theme="danger" label="Batal" data-dismiss="modal"/>
                                </form>
                            </x-slot>
                            </x-adminlte-modal>
                    </td>

                

                         </tr>
                       @endforeach
               </tbody>
           </table>
       </div>
   </div>
</div>


@stop
@section('plugins.Datatables', true)
@section('js')
   <script> $('#tabel_spp').DataTable();</script>
@stop