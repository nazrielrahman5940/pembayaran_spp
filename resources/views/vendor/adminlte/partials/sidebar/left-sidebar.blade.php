<aside class="main-sidebar {{ config('adminlte.classes_sidebar', 'sidebar-dark-primary elevation-4') }}">

    {{-- Sidebar brand logo --}}
    @if (config('adminlte.logo_img_xl'))
        @include('adminlte::partials.common.brand-logo-xl')
    @else
        @include('adminlte::partials.common.brand-logo-xs')
    @endif
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
        integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.8.1/font/bootstrap-icons.min.css" integrity="sha512-Oy+sz5W86PK0ZIkawrG0iv7XwWhYecM3exvUtMKNJMekGFJtVAhibhRPTpmyTj8+lJCkmWfnpxKgT2OopquBHA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    {{-- Sidebar menu --}}
    <div class="sidebar">
        <nav class="pt-2">
            <ul class="nav nav-pills nav-sidebar flex-column {{ config('adminlte.classes_sidebar_nav', '') }}"
                data-widget="treeview" role="menu"
                @if (config('adminlte.sidebar_nav_animation_speed') != 300) data-animation-speed="{{ config('adminlte.sidebar_nav_animation_speed') }}" @endif
                @if (!config('adminlte.sidebar_nav_accordion')) data-accordion="false" @endif>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">
                        <p style="text-align:center;font-size:20px"> Dashboard </P>
                    </a>
                </li>
                {{-- <li class="nav-item">
                       <a class="nav-link" href="{{route('home')}}">
                        <i class="fas fa-user"></i>
                          <p>Dahsboard</p>
                       </a>
                   </li> --}}
                <li class="nav-header">MENU</li>

                {{-- jika session is_siswa ga ada  (artinya dia level admin dan petugas maka masuk klausa dibawah) --}}
                @if (!session()->has('is_siswa'))
                    @if (\Auth::user()->level == 'admin')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('siswa.index') }}">
                                <i class="bi bi-people-fill"></i>
                                <p> Data Siswa </P>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('user.index') }}">
                                <i class="bi bi-person-badge"></i>
                                <p> Data Petugas </P>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('kelas.index') }}">
                                <i class="bi bi-person-video3"></i>
                                <p> Data Kelas </P>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('spp.index') }}">
                                <i class="bi bi-currency-bitcoin"></i>
                                <p> Data Spp </P>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('pembayaran.index') }}">
                                <i class="bi bi-credit-card"></i>
                                <p> Entri Pembayaran </P>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('histori.index') }}">
                                <i class="bi bi-hourglass-split"></i>
                                <p> Histori Pembayaran </P>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('laporan.index') }}">
                                <i class="bi bi-envelope-paper"></i>
                                <p> Generate Laporan </P>
                            </a>
                        </li>
                    @elseif(\Auth::user()->level == 'petugas')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('user.index') }}">
                                <i class="far fa-fw fa-user"></i>
                                <p> user </P>
                            </a>
                        </li>
                    @else(\Auth::user()->level=='owner')
                    @endif
                @else
                    {{-- kalau dia siswa maka masuk sini --}}
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('siswa.histori') }}">
                            <i class="fas fa-dollar-sign"></i>
                            <p> Histori Pembayaran </P>
                        </a>
                    </li>
                @endif

            </ul>
        </nav>
    </div>

</aside>
