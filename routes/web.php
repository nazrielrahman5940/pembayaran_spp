<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/welcome');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [
    App\Http\Controllers\HomeController::class, 'index'
])->middleware(['checkrole:admin,petugas,siswa'])->name('home');

// Route::get('/home', function () {
//     return view('home');
// })->name('home')->middleware(['checkrole:admin,petugas,siswa']);

Route::group(['prefix' => 'user', 'as' => 'user.'], function () {

    Route::group(['middleware' => 'checkrole:admin'], function () {
        Route::get('/', [
            App\Http\Controllers\UserController::class, 'index'
        ])->name('index');

        Route::get('/create', [
            App\Http\Controllers\UserController::class, 'create'
        ])->name('create');

        Route::post('/store', [
            App\Http\Controllers\UserController::class, 'store'
        ])->name('store');

        Route::get('/edit/{id}', [
            App\Http\Controllers\UserController::class, 'edit'
        ])->name('edit');

        Route::put('/{id}', [
            App\Http\Controllers\UserController::class, 'update'
        ])->name('update');

        Route::get('/show/{id}', [
            App\Http\Controllers\UserController::class, 'show'
        ])->name('show');

        Route::delete('/{id}', [
            App\Http\Controllers\UserController::class, 'destroy'
        ])->name('delete');
    });
});


Route::group(['prefix' => 'siswa', 'as' => 'siswa.'], function () {
    // Route::get('/login', App\Http\Controllers\SiswaLoginController::class, 'login')->name('login');

    Route::get('/login', [
        'uses' => 'App\Http\Controllers\SiswaLoginController@index',
        'as' => 'login'
    ]);

    Route::post('/action-login', [
        'uses' => 'App\Http\Controllers\SiswaLoginController@actionlogin',
        'as' => 'action-login'
    ]);

    Route::get('/histori', [
        'uses' => 'App\Http\Controllers\historiController@index',
        'as' => 'histori'
    ])->middleware('checkrole:admin,petugas,siswa');

    // Route::middleware('middleware' => 'checkrole:admin')
    Route::group(['middleware' => 'checkrole:admin'], function () {
        Route::get('/', [
            App\Http\Controllers\SiswaController::class, 'index'
        ])->name('index');

        Route::get('/create', [
            App\Http\Controllers\SiswaController::class, 'create'
        ])->name('create');

        Route::post('/store', [
            App\Http\Controllers\SiswaController::class, 'store'
        ])->name('store');

        Route::get('/edit/{id}', [
            App\Http\Controllers\SiswaController::class, 'edit'
        ])->name('edit');

        Route::put('/{id}', [
            App\Http\Controllers\SiswaController::class, 'update'
        ])->name('update');

        Route::get('/show/{id}', [
            App\Http\Controllers\SiswaController::class, 'show'
        ])->name('show');

        Route::delete('/{id}', [
            App\Http\Controllers\SiswaController::class, 'destroy'
        ])->name('delete');
    });
});

Route::group(['prefix' => 'kelas', 'as' => 'kelas.'], function () {

    Route::group(['middleware' => 'checkrole:admin'], function () {
        Route::get('/', [
            App\Http\Controllers\KelasController::class, 'index'
        ])->name('index');

        Route::get('/create', [
            App\Http\Controllers\KelasController::class, 'create'
        ])->name('create');

        Route::post('/store', [
            App\Http\Controllers\KelasController::class, 'store'
        ])->name('store');

        Route::get('/edit/{id}', [
            App\Http\Controllers\KelasController::class, 'edit'
        ])->name('edit');

        Route::put('/{id}', [
            App\Http\Controllers\KelasController::class, 'update'
        ])->name('update');

        Route::get('/show/{id}', [
            App\Http\Controllers\KelasController::class, 'show'
        ])->name('show');

        Route::delete('/{id}', [
            App\Http\Controllers\KelasController::class, 'destroy'
        ])->name('delete');
    });
});

Route::group(['prefix' => 'spp', 'as' => 'spp.'], function () {

    Route::group(['middleware' => 'checkrole:admin'], function () {
        Route::get('/', [
            App\Http\Controllers\SppController::class, 'index'
        ])->name('index');

        Route::get('/create', [
            App\Http\Controllers\SppController::class, 'create'
        ])->name('create');

        Route::post('/store', [
            App\Http\Controllers\SppController::class, 'store'
        ])->name('store');

        Route::get('/edit/{id}', [
            App\Http\Controllers\SppController::class, 'edit'
        ])->name('edit');

        Route::put('/{id}', [
            App\Http\Controllers\SppController::class, 'update'
        ])->name('update');

        Route::get('/show/{id}', [
            App\Http\Controllers\SppController::class, 'show'
        ])->name('show');

        Route::delete('/{id}', [
            App\Http\Controllers\SppController::class, 'destroy'
        ])->name('delete');
    });
});

Route::group(['prefix' => 'pembayaran', 'as' => 'pembayaran.'], function () {

    Route::group(['middleware' => 'checkrole:admin'], function () {
        Route::get('/', [
            App\Http\Controllers\PembayaranController::class, 'index'
        ])->name('index');

        Route::get('/create', [
            App\Http\Controllers\PembayaranController::class, 'create'
        ])->name('create');

        Route::post('/store', [
            App\Http\Controllers\PembayaranController::class, 'store'
        ])->name('store');

        Route::get('/edit/{id}', [
            App\Http\Controllers\PembayaranController::class, 'edit'
        ])->name('edit');

        Route::put('/{id}', [
            App\Http\Controllers\PembayaranController::class, 'update'
        ])->name('update');

        Route::get('/show/{id}', [
            App\Http\Controllers\PembayaranController::class, 'show'
        ])->name('show');

        Route::delete('/{id}', [
            App\Http\Controllers\PembayaranController::class, 'destroy'
        ])->name('delete');
        // Route::get('/laporan', [
        //     App\Http\Controllers\PembayaranController::class, 'laporan'
        // ])->name('laporan');
        // Route::get('/laporan/{tglawal}/{tglakhir}', [
        //     App\Http\Controllers\PembayaranController::class, 'cetakpdf'
        // ])->name('laporan.cetak');
        
    });
});


Route::get('/histori', [
    App\Http\Controllers\historiController::class, 'index'
])->middleware(['checkrole:admin,petugas'])->name('histori.index');

Route::get('/generate-laporan', [
    App\Http\Controllers\PembayaranController::class, 'generatelaporan'
])->middleware(['checkrole:admin'])->name('laporan.index');
Route::get('/generate-laporan-cetak/{tglawal}/{tglakhir}', [
    App\Http\Controllers\PembayaranController::class, 'generatelaporancetak'
])->middleware(['checkrole:admin'])->name('generate-laporan-cetak');
